# Template Exemplo para integração da Absam com seu projeto em Django no Gitlab.

A absam é uma plataforma de cloud servers e cloud-app's que facilita o desenvolvimento da sua aplicação, e implantação, temos templates dos frameworks mais utilizados no mercado, além de suporte especializado 24hrs, temos a modalidade de cobrança por hora.

para mais informações:

http://absam.io/

## Usage

https://ppcardoso.medium.com/como-integrar-gitlab-ci-com-a-absam-83a6347d081c


## Contributing
Pull requests são bens vindos.

Para mudanças importantes, abra um problema primeiro para discutir o que você gostaria de mudar.

## License
[MIT](https://choosealicense.com/licenses/mit/)